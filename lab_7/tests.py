from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, friend_list, get_friend_list, delete_friend
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import json
from collections import namedtuple


#mengambil data yang tersedia untuk testcase
def initiate_friend():
    page = 5
    response = Client().post('/lab-7/change-page/', {'page':page})
    listOfMhs = json.loads(json.loads(response.content.decode('utf-8'))['listOfMhs'])
    friend = listOfMhs[0]
    return friend

# Create your tests here.
class Lab7UnitTest(TestCase):

    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_get_friend_list_data_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_auth_param_dict(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])

    def test_add_friend(self):
        response_post = Client().post(
            '/lab-7/add-friend/',
            {'name': "hayosiapa", 'npm': "1606474747"}
        )
        self.assertEqual(response_post.status_code, 200)

    def test_lab_7_can_delete_object(self):
        Friend.objects.create(friend_name='Tamamo', npm='1707060600')
        friend_obj = Friend.objects.all()[0]
        response = Client().post('/lab-7/delete-friend/', {'id': friend_obj.id})
        self.assertEqual(Friend.objects.all().count(), 0)

    def test_lab_7_can_validate_existing_npm(self):
        Friend.objects.create(friend_name='MHX', npm='1234567890')
        friend_obj = Friend.objects.all()[0]
        response = Client().post('/lab-7/validate-npm/', {'npm': friend_obj.npm})
        respDict = json.loads(response.content.decode('utf-8'))
        self.assertTrue(respDict['is_taken'])

    def test_lab_7_can_validate_unpresent_npm(self):
        response = Client().post('/lab-7/validate-npm/', {'npm': '1001'})
        respDict = json.loads(response.content.decode('utf-8'))
        self.assertFalse(respDict['is_taken'])

    def test_model_can_create_new_friend(self):
        friend = initiate_friend()
        Friend.objects.create(friend_name=friend['nama'], npm=friend['npm'])
        self.assertEqual(Friend.objects.all().count(), 1)

    def test_model_can_create_from_addfriend(self):
        friend = initiate_friend()
        response = Client().post('/lab-7/add-friend/', {'name': friend['nama'], 'npm': friend['npm']})
        self.assertEqual(Friend.objects.all().count(), 1)
        responseFriendList = Client().get('/lab-7/get-friend-list/')
        listOfMhs = json.loads(responseFriendList.content.decode('utf-8'))
        self.assertEqual(listOfMhs['friend_list'][0]['friend_name'], friend['nama'])

    def test_lab_7_loading_fails_with_wrong_credential(self):
        wrong_name = "penyusup"
        wrong_password = "malingduit"
        try:
            csui_helper = CSUIhelper(wrong_name, wrong_password)
        except Exception as e:
            self.assertIn("username atau password sso salah", str(e))
            self.assertIn(wrong_name, str(e))
            self.assertIn(wrong_password, str(e))
